var express          = require ('express');
var path             = require ('path');
var favicon          = require ('serve-favicon');
var logger           = require ('morgan');
var cookieParser     = require ('cookie-parser');
var bodyParser       = require ('body-parser');
var load             = require ('express-load');
var expressSession   = require ('express-session');
var passportLocal    = require ('passport-local');
var passport         = require ('passport');
var passportHttp     = require ('passport-http');
var flash            = require ('express-flash');
var FacebookStrategy = require ('passport-facebook').Strategy

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser('iY}ONxQ,Y9I^Z}&y6-i}~35cS/vk/sf8+y@8c.2></>P*Z03Xhue?lzY%|dzN>S'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());
app.use(expressSession({
  secret: process.env.SESSION_SECRET || '1a5H(qzO&1+!8M35tXvai3A*JF%Os]eOoG63/Oo+:1S(R[%x[js09UKDam0#85',
  saveUninitialized: false,
  resave: false,
  cookie: {
    httpOnly: true,
  }
}));
app.use(passport.initialize());
app.use(passport.session());


load('models', {cwd: 'app'})
.then('controllers')
.then('funcao')
.then('middleware')
.then('routes')
.then('config')
.into(app);

/*
// Facebook
passport.use(new FacebookStrategy({
  clientID: '322727351440354',
  clientSecret: '17da61c0ad756bff0c77b6e91861bfea',
  callbackURL: "http://localhost:3000/login/facebook"
},
function(accessToken, refreshToken, profile, done) {
  // // var usuario = app.models.usuario;
  // // usuario.update({ facebookId: profile.id, nome: profile.nome }, function (err, user) {

    return done(null, profile);
  // // });
}
));
*/
//Normal
passport.use(new passportLocal.Strategy({
  usernameField: 'login',
  passwordField: 'senha'

},verificaLogin));
passport.use(new passportHttp.BasicStrategy(verificaLogin));

function verificaLogin(username, password, done){
 
  var pass = require('./app/middleware/password');
  var User = app.models.users;
  User.findOne({'login': username }, function (err, result) {

    if(err) { console.log("ERROR: " + err); }
    else {
      if(result){
        if(result.login == username && pass.validate(result.password, password)) {
          done(null, result);
        } else {
          done(null, null);
        }
      } else {

        done(null, null);
      }
    }
  });
}

passport.serializeUser(function(user, done) {

  done(null, user);
});

passport.deserializeUser(function(user, done) {

  done(null, user);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
