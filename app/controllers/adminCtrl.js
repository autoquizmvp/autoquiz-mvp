module.exports = function(app){

	var adminCtrl = {
		aluno: function(req,res){ //
			res.render('painel/aluno');
		},
		autoEscola: function(req,res){
			res.render('painel/autoEscola');
		}, 
		professor: function(req,res){
			res.render('painel/professor')
		},
		questoes: function(req,res){
			res.render('painel/questoes')
		}
	} 
	return adminCtrl;
}