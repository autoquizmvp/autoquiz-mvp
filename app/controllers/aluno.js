module.exports = function(app){
	var Aluno = app.models.alunos;

	alunoCtrl = {
		buscarPopulate : function(req, res) {
			Aluno.find()
			.populate('professor')
			.populate('turma')
			.exec(function(err, aluno) {
				if(!err) {
					res.json(aluno);
				} else {
					console.log(err);
				}
			})
		}
	}
	return alunoCtrl;
}