module.exports = function (app){

	var loginCtrl ={
		logout: function(req,res){
			req.logout();
			res.redirect('/');
		},
		index: function(req,res){
			res.redirect('/home');
		},
		render: function(req, res){
			var data = {user : req.user}
			var teste = [];
			teste.push(data);
			res.render('painel/index', data);
		},
		loginErr:function(req, res) {
			console.log('estou na /rota');
			req.flash('info', 'Login ou senha incorretos');
			res.redirect('/');
		}
	}
	return loginCtrl;
}