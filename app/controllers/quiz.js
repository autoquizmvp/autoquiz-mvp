module.exports = function(app){
	var Quiz   = app.models.quiz;
	var unique = require('array-unique');

	var AutoQuizCtrl = {
		painel: function(req,res){
			res.render('painel/questoes');
		},
		facebook: function(req,res){
			res.redirect('/responder/quiz');
		},
		escolha:function(req,res){
			res.render('quiz/escolha');//temporario
		}, 
		quiz: function(req,res){
			res.render('quiz/content');
		},
		login: function(req,res){
			res.render('quiz/login');
		},
		cadastrar: function(req,res){
			console.log('cheguei aqui');
			var quiz = new Quiz;
			quiz.foto         = req.body.foto;
			quiz.enunciado    = req.body.enunciado;
			quiz.alternativaA = req.body.alternativaA;
			quiz.alternativaB = req.body.alternativaB;
			quiz.alternativaC = req.body.alternativaC;
			quiz.alternativaD = req.body.alternativaD;
			quiz.alternativaE = req.body.alternativaE;
			quiz.alternativaCorreta = req.body.alternativaCorreta;
			quiz.categoria    = req.body.categoria;
			quiz.save(function(err, quiz){
				if(!err){
					console.log('certo!!');
					console.log(quiz);
				} else {
					console.log('erro!!');
					console.log(err);	
				}
			})
		},
		listarRandom: function(req,res){

			var categorias = req.body.categorias;
			var qtdArray = [];
			var resposta = [];
			var skip = 0;
			var qtdQuest = 40;

			var qtd = Math.round(qtdQuest/categorias.length)*categorias.length>qtdQuest ? Math.floor(qtdQuest/categorias.length) : Math.round(qtdQuest/categorias.length);

			console.log(qtd);

			categorias.forEach(function(){ qtdArray.push(qtd) })

			var soma = qtd*categorias.length;
			var sub = qtdQuest-soma;

			if(soma < qtdQuest) {
				do {
					var index = Math.round(Math.random()*(categorias.length-1));
					console.log(index);
					qtdArray[index] = qtdArray[index]+1;
					soma = soma+1;
					sub = qtdQuest-soma;
				}
				while (soma!=qtdQuest);
			}

			var skip = [];

			skip.push(Math.ceil(Math.random()*soma));

			while (skip.length < soma) {
				var sorte = Math.ceil(Math.random()*soma);
				while (skip.indexOf(sorte) != -1) {
					sorte = Math.ceil(Math.random()*soma);
				}
				skip.push(sorte);
			}

			var result = [];
			var count = 0;
			var num = 0;

			for(i=0 ; i < categorias.length ; i++) {
				for(j = 0 ; j < qtdArray[i] ; j++) {
					num++;
					console.log(num);
					Quiz.findOne({categoria : categorias[i]}).skip(skip[num-1]).exec(function(err, quiz){
						if(!err){
							result = result.concat(quiz);
						} else {
							console.log(err);
						}
					}).then(function(){
						count++;
						if(count == soma) {
							count=0;
							res.json(result);
						}
					})
				}
			}
		},
		listar: function(req,res){
			//41, 29, 42, 30, 51, 22
			Quiz.find(function(err, quiz){
				if(!err){
					res.json(quiz);
				} else {
					console.log(err);
				}
			})
		},
		editar: function(req,res){
			var quiz          = new Quiz();
			quiz._id          = req.body._id;
			quiz.foto         = req.body.foto;
			quiz.enunciado    = req.body.enunciado;
			quiz.alternativaA = req.body.alternativaA;
			quiz.alternativaB = req.body.alternativaB;
			quiz.alternativaC = req.body.alternativaC;
			quiz.alternativaD = req.body.alternativaD;
			quiz.alternativaE = req.body.alternativaE;
			quiz.alternativaCorreta = req.body.alternativaCorreta;
			quiz.categoria    = req.body.categoria;

			var where = {_id : quiz._id};
			var update = {$set: {
				foto:         quiz.foto,
				enunciado:    quiz.enunciado,
				alternativaA: quiz.alternativaA,
				alternativaB: quiz.alternativaB,
				alternativaC: quiz.alternativaC,
				alternativaD: quiz.alternativaD,
				alternativaCorreta: quiz.alternativaCorreta,
				categoria:    quiz.categoria
			}};

			Quiz.update(where,update,function(err, quiz){
				console.log('entrou no editar');
				if(!err){
					res.json(quiz);
				} else {
					console.log(err);
				}
			})
		}, 
		editById: function(req, res){
			Quiz.findById({_id: req.params.id}, function(err, quiz){
				if(!err){
					res.json(quiz);
				} else {
					console.log(err);
				}
			})
		},
		deletar: function(req,res){
			Quiz.remove({_id: req.params.id}, function(err, quiz){
				if(!err){
					res.json(quiz);
				} else {
					console.log(err);
				}
			})
		}
	}
	return AutoQuizCtrl;
}
