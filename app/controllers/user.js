module.exports = function (app){
	var User       = app.models.users;
	var AutoEscola = app.models.autoEscola;
	var Professor  = app.models.professor;

	var userCtrl = {	
		listarAlunos : function(req, res){
			User.findById({_id: req.params.id}, function(err, user){
				if(!err){
					if(user.tipo == 0){
						res.redirect('/listarAlunos');
					} else if(user.tipo == 1){
						AutoEscola.findById({user: user._id}, function(err, autoEscola){
							if(!err){
								res.redirect('/aluno/autoEscola/'+AutoEscola._id);
							} else {
								console.log(err);
							}
						})
					} else if(user.tipo == 2){
						Professor.findById({user: user._id}, function(err, professor){
							if(!err){
								res.redirect('/aluno/professor/'+professor._id);
							} else {
								console.log(err);
							}
						})
					}
				} else {
					console.log(err);
				}
			})
		},
		listarProfessores : function(req, res){
			User.findById({_id: req.params.id}, function(err, user){
				if(!err){
					if(user.tipo == 0){
						res.redirect('/listarProfessores');
					} else if(user.tipo == 1){
						AutoEscola.findById({user: user._id}, function(err, autoEscola){
							if(!err){
								res.redirect('/professor/autoEscola/'+AutoEscola._id);
							} else {
								console.log(err);
							}
						})
					}
				} else {
					console.log(err);
				}
			})
		},
		listarTurmas : function(req, res){
			User.findById({_id: req.params.id}, function(err, user){
				if(!err){
					if(user.tipo == 0){
						res.redirect('/listarTurmas');
					} else if(user.tipo == 1){
						AutoEscola.findById({user: user._id}, function(err, autoEscola){
							if(!err){
								res.redirect('/turma/autoEscola/'+AutoEscola._id);
							} else {
								console.log(err);
							}
						})
					}
				} else {
					console.log(err);
				}
			})
		}
	}
	return userCtrl;
}