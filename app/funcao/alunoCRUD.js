module.exports = function(app){
	var Aluno    = app.models.alunos;
	var Simulado = app.models.simulados;
	var User     = app.models.users;

	var  alunoCtrl = {
		cadastrar: function(req,res){
			var aluno       = new Aluno();
			aluno.autoEscola = req.body.autoEscola;
			aluno.turma     = req.body.turma;
			aluno.user 		= req.body.user;
			aluno.nome      = req.body.nome;
			aluno.telefone  = req.body.telefone;
			aluno.cpf 		= req.body.cpf;
			aluno.endereco  = req.body.endereco;
			aluno.email 	= req.body.email;
			aluno.save(function(err,aluno){
				if(!err){
					res.json(aluno);
					console.log('cadastrou');
				} else {
					res.json(req.body);
					console.log(err);
				}
			})
		},
		editar: function(req,res){
			var aluno   = new Aluno();
			aluno._id   = req.body._id;
			aluno.nome      = req.body.nome;
			aluno.telefone  = req.body.telefone;
			aluno.cpf 		= req.body.cpf;
			aluno.endereco  = req.body.endereco;
			aluno.email 	= req.body.email;
			aluno.turma     = req.body.turma;


			var where   = {_id : aluno._id};
			var update  = {$set:{
				nome:     aluno.nome,
				telefone: aluno.telefone,
				cpf:      aluno.cpf,
				endereco: aluno.endereco,
				email:    aluno.email,
				turma:    aluno.turma,
				professor: aluno.professor
			}};
			
			Aluno.update(where, update, function(err, aluno){
				console.log('auterou');
				if(!err){
					res.json(aluno);
				} else {
					console.log(err);
				}
			})

		},
		listar: function(req,res){
			Aluno.find(function(err,aluno){
				if(!err){
					res.json(aluno);
				} else {
					console.log(err);
				}
			})
		},
		deletar: function(req,res){
			Aluno.findById({_id:req.params.id}, function(err,aluno){
				if(!err){
					User.remove({_id:aluno.user}, function(err, user){
						if(!err){
							Aluno.remove({_id: aluno._id}, function(err, aluno){
								res.json(aluno);
							})
						}
					})
				} else {
					res.json(err);
				}
			})
		},
		listarById: function(req, res){
			Aluno.findById({_id: req.params.id})
			.populate('user')
			// .populate('professor')
			// .populate('turma')
			.exec(function(err, aluno){
				if(!err){
					console.log("Buscando aluno");
					console.log(aluno);
					res.json(aluno);
				} else {
					console.log(err);
				}
			})
		},
		listarByUser: function(req, res){
			Aluno.findOne({user: req.params.id}, function(err, aluno){
				if(!err){
					res.json(aluno);
				} else {
					console.log(err);
				}
			})
		},
		listarByAuto: function(req, res){
			Aluno.find({autoEscola: req.params.id}, function(err, aluno){
				if(!err){
					res.json(aluno);
				} else {
					console.log(err);
				}
			})
		},
		listarByProfessor: function(req, res){
			Aluno.find({professor: req.params.id}, function(err, aluno){
				if(!err){
					res.json(aluno);
				} else {
					console.log(err);
				}
			})
		}
	}
	return alunoCtrl;
}