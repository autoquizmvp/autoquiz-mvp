module.exports = function(app){
	var AutoEscola = app.models.autoEscola;
	var User       = app.models.users;

	var  autoEscolaCtrl = {
		cadastrar: function(req,res){
			var autoEscola    = new AutoEscola();
			autoEscola.user     = req.body.user;
			autoEscola.nome     = req.body.nome;
			autoEscola.cnpj     = req.body.cnpj;
			autoEscola.endereco = req.body.endereco;
			autoEscola.telefone = req.body.telefone;
			autoEscola.save(function(err, autoEscola){
				if(!err){
					res.json(autoEscola);
					console.log('cadastrou');
				} else {
					res.json(req.body);
					console.log(err);
				}
			})
		},
		editar: function (req,res){
			var autoEscola   = new AutoEscola();
			autoEscola._id      = req.body._id;
			autoEscola.nome     = req.body.nome;
			autoEscola.cnpj     = req.body.cnpj;
			autoEscola.endereco = req.body.endereco;
			autoEscola.telefone = req.body.telefone;

			
			var where  = {_id : autoEscola._id};
			var update = {$set:{
				nome:     autoEscola.nome,
				cnpj:     autoEscola.cnpj,
				endereco: autoEscola.endereco,
				telefone: autoEscola.telefone,
				
			}};
			AutoEscola.update(where, update, function(err, autoEscola){
				console.log('auterou');
				if(!err){
					res.json(autoEscola);
				}else{
					console.log(err);
				}
			})

		},
		listar: function(req,res){
			AutoEscola.find(function(err,autoEscola){
				if(!err){
					res.json(autoEscola);
				}else{
					res.json(err);
				}
			})
		},
		deletar: function(req,res){
			AutoEscola.findById({_id:req.params.id}, function(err,autoEscola){
				if(!err){
					User.remove({_id: autoEscola.user}, function(err, user){
						if(!err){
							AutoEscola.remove({_id: autoEscola._id}, function(err, autoEscola){
								res.json(autoEscola);
							})
						}
					})
				} else {
					res.json(err);
				}
			})
		},
		listarById: function(req, res){
			AutoEscola.findById({_id: req.params.id})
			.populate('user')
			.exec(function(err, autoEscola){
				if(!err){
					res.json(autoEscola);
				} else {
					console.log(err);
				}
			})
		},
		listarByUser: function(req, res){
			AutoEscola.findOne({user: req.params.id}, function(err, autoEscola){
				if(!err){
					res.json(autoEscola);
				} else {
					console.log(err);
				}
			})
		}
	}
	return autoEscolaCtrl;
}
