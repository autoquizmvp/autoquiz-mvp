module.exports = function(app) {
	var Nota = app.models.notas;
	var notaCtrl = {
		cadastrar : function(req, res){
			var nota = new Nota();
			nota.nota       = req.body.nota;
			nota.aluno      = req.body.aluno;
			nota.autoEscola = req.body.autoEscola;
			nota.simulado   = req.body.simulado;
			nota.save(function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})
		}, 
		editar : function(req, res){
			var nota = new Nota();
			nota._id  = req.body._id;
			nota.nota = req.body.nota;
			var where = {_id : nota._id};
			var update = {$set: {
				nota : nota.nota
			}}
			nota.update(where, update, function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})

		},
		delete : function(req, res){
			Nota.remove({_id : req.params.id}, function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})
		},
		lista : function(req, res){
			Nota.find(function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})
		},
		listarBySimulado : function(req, res){
			Nota.find({simulado: req.params.id})
			.populate('autoEscola')
			.populate('aluno')
			.exec(function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})
		},
		listarByAutoEscola : function(req, res){
			Nota.find({autoEscola: req.params.id})
			.populate('aluno')
			.populate('simulado')
			.exec(function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})
		},
		listarByAluno : function(req, res){
			Nota.find({aluno: req.params.id})
			.populate('aluno')
			.populate('simulado')
			.exec(function(err, nota){
				if(!err){
					res.json(nota);
				} else {
					console.log(err);
				}
			})
		}
	}
	return notaCtrl;
}