module.exports = function(app){
	var Professor = app.models.professor;
	var AutoEscola = app.models.autoEscola;
	var User     = app.models.users;

	var  professorCtrl = {
		cadastrar: function(req,res){
			var professor   = new Professor();
			professor.autoEscola = req.body.autoEscola;
			professor.user       = req.body.user;
			professor.nome       = req.body.nome;
			professor.telefone   = req.body.telefone;
			professor.endereco   = req.body.endereco;
			professor.cpf 	     = req.body.cpf;
			professor.email      = req.body.email;
			professor.save(function(err,professor){
				if(!err){
					res.json(professor);
					console.log(professor);
					console.log('cadastrou');
				} else {
					res.json(req.body);
					console.log(err);
				}
			})
		},
		editar: function (req,res){
			var professor   = new Professor();
			professor._id      = req.body._id;
			professor.nome     = req.body.nome;
			professor.telefone = req.body.telefone;
			professor.endereco = req.body.endereco;
			professor.cpf 	   = req.body.cpf;
			professor.email    = req.body.email;


			var where  = {_id : professor._id};
			var update = {$set:{
				nome:     professor.nome,
				telefone: professor.telefone,
				endereco: professor.endereco,
				cpf: 	  professor.cpf,
				email:    professor.email

			}};
			Professor.update(where, update, function(err, professor){
				console.log('alterou');
				if(!err){
					res.json(professor);
				} else {
					console.log(err);
				}
			})

		},
		listar: function(req,res){
			Professor.find().populate('autoEscola').exec(function(err,professor){
				if(!err){
					res.json(professor);
				} else {
					res.json(err);
				}
			})
		},
		deletar: function(req,res){
			Professor.findById({_id:req.params.id}, function(err,professor){
				if(!err){
					User.remove({_id:professor.user}, function(err, user){
						if(!err){
							Professor.remove({_id: professor._id}, function(err, professor){
								res.json(professor);
							})
						}
					})
				} else {
					res.json(err);
				}
			})
		},
		listarById: function(req, res){
			Professor.findById({_id: req.params.id})
			.populate('user')
			.populate('autoEscola')
			.exec(function(err, professor){
				if(!err){
					res.json(professor);
				} else {
					console.log(err);
				}
			})
		},
		editById: function(req, res){
			Professor.findById({_id: req.params.id})
			.populate('user')
			.exec(function(err, professor){
				if(!err){
					res.json(professor);
				} else {
					console.log(err);
				}
			})
		},
		listarByUser: function(req, res){
			Professor.findOne({user: req.params.id})
			.populate('autoEscola')
			.exec(function(err, professor){
				if(!err){
					console.log(professor);
					res.json(professor);
				} else {
					console.log(err);
				}
			})
		},
		listarByAuto: function(req, res){
			Professor.find({autoEscola: req.params.id}).populate('autoEscola').exec(function(err, professor){
				if(!err){
					res.json(professor);
				} else {
					console.log(err);
				}
			})
		}
	}
	return professorCtrl;
}