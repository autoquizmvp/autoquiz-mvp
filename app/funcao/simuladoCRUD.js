module.exports = function (app) {
	var Simulado = app.models.simulados;
	var simuladoCtrl = {
		cadastrar : function(req, res){
			var simulado = new Simulado;
			simulado.nome = req.body.nome;
			simulado.turma = req.body.turma;
			simulado.professor = req.body.professor;
			simulado.autoEscola = req.body.autoEscola;
			simulado.questoes = req.body.questoes;
			simulado.save(function(err, simulado){
				if(!err){
					res.json(simulado);
					console.log(simulado);
				} else {
					console.log(err);
				}
			})
		},
		listar : function(req, res){
			Simulado.find(function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		},
		listarById : function(req, res){
			Simulado.findById({_id : req.params.id}, function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		},
		listarByTurma : function(req, res){
			Simulado.find({turma : req.params.id}, function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		},
		listarByAutoEscola : function(req, res){
			Simulado.find({autoEscola : req.params.id}, function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		},
		listarByProf : function(req, res){
			Simulado.find({professor : req.params.id}, function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		},
		editar : function(req, res){
			var simulado = new Simulado();
			simulado.nome = req.body.nome;
			simulado.questoes = req.body.questoes;
			var where = {_id: req.params.id};
			var update = {$set: {
				nome: simulado.nome,
				questoes: simulado.questoes
			}}
			simulado.update(where, update, function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		},
		deletar : function(req, res){
			Simulado.remove({_id: req.params.id}, function(err, simulado){
				if(!err){
					res.json(simulado);
				} else {
					console.log(err);
				}
			})
		}
	}
	return simuladoCtrl;
}