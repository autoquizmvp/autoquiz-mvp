module.exports = function(app){
	var Turma = app.models.turma;

	var  turmaCRUDCtrl = {	
		cadastrar: function(req,res){
			var turma   = new Turma();
			turma.nome          = req.body.nome;
			turma.autoEscola    = req.body.autoEscola;
			turma.professor     = req.body.professor;
			turma.dataDeInicio  = req.body.dataDeInicio;
			turma.dataDeFim     = req.body.dataDeFim;
			turma.save(function(err,turma){
				if(!err){
					res.json(turma);
					console.log('cadastrou');
				} else {
					res.json(req.body);
					console.log(err);
				}
			})
		},
		editar: function(req,res){
			var turma   = new Turma();
			turma._id   = req.body._id;
			turma.nome         = req.body.nome;
			turma.dataDeInicio = req.body.dataDeInicio;
			turma.dataDeFim    = req.body.dataDeFim;
			turma.professor    = req.body.professor;

			var where  = {_id : turma._id};
			var update = {$set:{
				nome         : turma.nome,
				dataDeInicio : turma.dataDeInicio,
				dataDeFim    : turma.dataDeFim,
				professor    : turma.professor
			}};
			Turma.update(where,update,function(err, turma){
				console.log('auterou');
				if(!err){
					res.json(turma);
				} else {
					console.log(err);
				}
			})
		},
		listar: function(req,res){
			Turma.find().populate('autoEscola').exec(function(err, turma){
				if(!err){
					res.json(turma);
				} else {
					res.json(err);
				}
			})
		},
		listarById: function(req, res){
			Turma.findById({_id : req.params.id}, function(err, turma){
				if(!err){
					res.json(turma);
				} else {
					console.log(err);
				}
			})
		},
		listarByProf: function(req, res){
			Turma.find({professor : req.params.id}).populate('autoEscola').exec(function(err, turma){
				if(!err){
					console.log(turma);
					res.json(turma);
				} else {
					console.log(err);
				}
			})
		},
		listarByAutoEscola: function(req, res){
			Turma.find({autoEscola : req.params.id}).populate('autoEscola').exec(function(err, turma){
				if(!err){
					console.log("Buscando turma by autoEscola server");
					res.json(turma);
				} else {
					console.log(err);
				}
			})
		},
		deletar: function(req,res){
			Turma.remove({_id:req.params.id}, function(err,turma){
				if(!err){
					res.json(turma);
				} else {
					console.log(err);
				}
			})
		}
	}
	return turmaCRUDCtrl;
}
