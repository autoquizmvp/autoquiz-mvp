module.exports = function (app){
	var User     = app.models.users;
	var Simulado = app.models.simulados;

	var userCtrl = {
		cadastrar: function(req,res){
			var user = new User();
			user.login    = req.body.login;
			user.password = req.body.password;
			user.tipo     = req.body.tipo;
			user.save(function(err, user){
				if(!err){
					res.json(user);
				} else {
					res.json(req.body);
				}
			});
		},
		editar:function(req,res){
			var user = new User();
			user._id      = req.body._id;
			user.login    = req.body.login;
			user.tipo     = req.body.tipo;

			var where  = {_id: user._id};
			var update = { 
				$set: {
					login:    user.login,
					tipo:     user.tipo
				}
			};

			User.update(where,update, function(err, user){
				console.log('editou');
				if(!err){
					res.json(user);
				} else {
					res.json(err);
					console.log(err);
				}
			})
		},
		listar:function(req,res){
			User.find(function(err,user){
				if(!err){
					res.json(user);
				} else {
					res.json(err);
					console.log(err);
				}
			}); 
		},
		listarById:function(req,res){
			User.findById({_id:req.params.id}, function(err,user){
				if(!err){
					res.json(user);
				} else {
					console.log(err);
				}
			})
		},
		deletar:function(req,res){
			User.remove({_id:req.params.id}, function(err,user){
				if(!err){
					console.log('deletou');
					res.json(user);
				} else {
					res.json(err);
					console.log(err);
				}
			});
		}
	}
	return userCtrl;
}