module.exports = function(app) {
	var mongoose = require('mongoose')
	,   Schema = mongoose.Schema
	, 	ObjectId = mongoose.Schema.ObjectId; 

	var autoEscola = new Schema({
		user:     {type: ObjectId, ref:'Users', required:true},
		nome:     {type:'String', required:true},
		cnpj:     {type:'String', required:true, unique:true}, 
		endereco: {type:'String', required:true},
		telefone: {type:'String', required:true}
	});
	return mongoose.model('AutoEscolas', autoEscola);
 }