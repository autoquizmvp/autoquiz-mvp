module.exports = function(app){
	var mongoose = require("mongoose")
	,   Schema   = mongoose.Schema
	,   ObjectId = Schema.ObjectId;

	var nota = new Schema ({
		aluno:      {type:ObjectId, ref:'Alunos', required:true},
		simulado:   {type:ObjectId, ref:'Simulado', required:true},
		autoEscola: {type:ObjectId, ref:'AutoEscolas', required:true},
		nota:       {type:Number, required:true}
	});

	return mongoose.model('Notas', nota);

}
