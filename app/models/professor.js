module.exports = function(app) {
	var mongoose = require('mongoose')
	,   Schema = mongoose.Schema
	, 	ObjectId = mongoose.Schema.ObjectId; 

	var professor = new Schema({
		autoEscola: {type: ObjectId, ref:'AutoEscolas', required:true},
		user: 	    {type: ObjectId, ref:'Users', required:true},
		nome: 	    {type:'String', required:true},
		telefone:   {type:'String', required:true},
		endereco:   {type:'String', required:true},
		cpf: 		{type:'String', required:true, unique:true},
		email:      {type:'String', required:false}
		//Tipo
		// 0  - admin
		// 1  - auto-escola
		// 2  - professor
		// 3  - aluno 	
	});
	return mongoose.model('Professores', professor);
}