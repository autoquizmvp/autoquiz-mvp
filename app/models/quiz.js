module.exports = function(app) {
	var mongoose = require('mongoose')
	,	Schema = mongoose.Schema;

	var quiz = new Schema({
		enunciado:    		{type: 'String', required: true},
		alternativaA: 		{type: 'String', required: true},
		alternativaB: 		{type: 'String', required: true},
		alternativaC: 		{type: 'String', required: true},
		alternativaD: 		{type: 'String', required: true},
		alternativaE: 		{type: 'String', required: true},
		alternativaCorreta: {type: 'String', required: true},
		categoria:  		{type: 'String', required: false},
		foto: 				{type: 'String'}
		//legenda- categoria
			// 1- Legislação,
			// 2- Sinalização||Placas,
			// 3- Direção Defentiva,
			// 4- Primeiro Socorros,
			// 5- Meio Ambiente e Cidadania,
			// 6- Noções de mecânica básica,
	});														
	return mongoose.model('Quiz', quiz);
}