module.exports = function(app) {
	var mongoose = require('mongoose')
	,	Schema = mongoose.Schema
	,   ObjectId = Schema.ObjectId;

	var simulado = new Schema({
		nome:       {type: 'String', required: true},
		autoEscola: {type: ObjectId, ref: 'autoEscolas', required: true},
		turma:      {type: ObjectId, ref: 'Turmas', required: true},
		professor:  {type: ObjectId, ref: 'Professores', required: false},
		questoes:   [],
		status:     {type: Number, required: true, default: 1},
		date:       {type: Date, default: Date.now}
		/*
			status:
				0 - inativo,
				1 - ativo
		*/
	});														
	return mongoose.model('Simulado', simulado);
}