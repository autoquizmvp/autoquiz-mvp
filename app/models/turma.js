module.exports = function(app) {
	var mongoose = require('mongoose')
	,   Schema   = mongoose.Schema
	, 	ObjectId = mongoose.Schema.ObjectId; 

	var turma = new Schema({
		nome:          {type: 'String'},
		autoEscola:    {type: ObjectId, ref:'AutoEscolas', required: true},
		professor:     {type: ObjectId, ref:'Professores', required: true},
		dataDeInicio:  {type: 'String', required: true},
		dataDeFim:     {type: 'String', required: true}
	});
	return mongoose.model('Turmas', turma);
 }