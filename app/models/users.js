module.exports = function(app) {
	var mongoose = require('mongoose')
	,   Schema   = mongoose.Schema
	,	pass = require('../middleware/password');

	function configPass (v) {
		return pass.hash(v);
	}

	var user = new Schema({
		login:     {type:'String', required:true},
		password:  {type:'String', required:true, set: configPass},
		tipo:      {type: Number, required:false}

		//admin 	  - 0 
		//Auto escola - 1
		//proff.      - 2
		//aluno		  - 3

	});
	return mongoose.model('Users', user);
 }