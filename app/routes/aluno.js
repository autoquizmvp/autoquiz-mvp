module.exports = function(app){
	var viewsCtrl = app.controllers.adminCtrl;
	var aluno     = app.funcao.alunoCRUD;
	var ctrl      = app.controllers.aluno;
	var user      = app.controllers.user;
	var autenticar = app.middleware.autenticador;
	
	//essa rota renderiza a views
	//app.get ('/aluno', viewsCtrl.aluno); //ok

	//CRUD
	app.get    ('/aluno',autenticar.loginSistema, aluno.listar); //ok
	app.post   ('/aluno', autenticar.loginSistema,aluno.cadastrar); //ok
	app.put    ('/aluno/',autenticar.loginSistema, aluno.editar); //ok
	app.delete ('/aluno/:id',autenticar.loginSistema, aluno.deletar); //ok
	app.get    ('/aluno/edit/:id',autenticar.loginSistema, aluno.listarById);
	app.get    ('/aluno/user/:id',autenticar.loginSistema, aluno.listarByUser);
	app.get    ('/aluno/:id', autenticar.loginSistema, aluno.listarById);
	app.get    ('/aluno/listar/:id',autenticar.loginSistema, user.listarAlunos);

	app.get    ('/aluno/autoEscola/:id',autenticar.loginSistema, aluno.listarByAuto);
	app.get    ('/aluno/professor/:id',autenticar.loginSistema, aluno.listarByProfessor);
	
 }