module.exports = function(app){
	var viewsCtrl = app.controllers.adminCtrl;
	var autoEscola = app.funcao.autoEscolaCRUD;
	var autenticar = app.middleware.autenticador;
	

	//resnderiza a view
	//app.get('/autoEscola',viewsCtrl.autoEscola);//ok


	//CRUD
	app.get    ('/autoEscola',autenticar.loginSistema, autoEscola.listar);//ok
	app.get    ('/autoEscola/edit/:id',autenticar.loginSistema, autoEscola.listarById);
	app.post   ('/autoEscola',autenticar.loginSistema, autoEscola.cadastrar);//ok
	app.put    ('/autoEscola',autenticar.loginSistema, autoEscola.editar);//ok
	app.delete ('/autoEscola/:id',autenticar.loginSistema, autoEscola.deletar);//ok
	app.get    ('/autoEscola/user/:id',autenticar.loginSistema, autoEscola.listarByUser);
	
 }