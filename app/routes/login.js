module.exports = function (app){
	var passport   = require('passport');
	var login      = app.controllers.login;
	var autenticar = app.middleware.autenticador;

	app.get('/rota',login.loginErr);
	app.get('/home', autenticar.loginSistema, login.render);

	app.post('/login', passport.authenticate('local',{failureRedirect:'/rota'}),
		 login.index);
	app.get('/logout', login.logout);
}