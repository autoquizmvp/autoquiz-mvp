module.exports = function (app) {
	var nota = app.funcao.notasCRUD;

	app.get    ('/nota/aluno/:id', nota.listarByAluno);
	app.get    ('/nota/simulado/:id', nota.listarBySimulado);
	app.get    ('/nota/autoEscola/:id', nota.listarByAutoEscola);
	app.get    ('/nota', nota.lista);
	app.delete ('/nota/:id', nota.delete);
	app.post   ('/nota', nota.cadastrar);
	app.put    ('/nota', nota.editar);

}