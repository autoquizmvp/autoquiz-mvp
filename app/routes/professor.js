module.exports = function(app){
	var professor = app.funcao.professorCRUD;
	var autenticar = app.middleware.autenticador;
	var user = app.controllers.user;

	//app.get   ('/professor', viwesCtrl.professor);//ok 
	app.get   ('/professor/lista/:id',autenticar.loginSistema, user.listarProfessores);
	//CRUD
	app.get   ('/professor/',autenticar.loginSistema,professor.listar);//ok
	app.post  ('/professor/',autenticar.loginSistema, professor.cadastrar);//ok
	app.put   ('/professor',autenticar.loginSistema, professor.editar);//ok
	app.delete('/professor/:id',autenticar.loginSistema, professor.deletar);//ok

	app.get   ('/professor/edit/:id',autenticar.loginSistema, professor.editById);
	app.get   ('/professor/user/:id',autenticar.loginSistema, professor.listarByUser);
	app.get   ('/professor/autoEscola/:id',autenticar.loginSistema, professor.listarByAuto);
	app.get   ('/professor/:id',autenticar.loginSistema, professor.listarById);

}