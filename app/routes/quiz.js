module.exports = function(app){
	var 	quiz 		= app.controllers.quiz
	,   	autenticar 	= app.middleware.autenticador
	,   	passport 	= require('passport')
	,  	    imag        = app.controllers.imag
	,       multer		= require('multer')
 	,       path 		= require('path')
 	,       crypto 		= require('crypto');

 	var storage = multer.diskStorage({
 		destination: 'public/imagens/',
 		filename: function (req, file, cb) {
 			crypto.pseudoRandomBytes(16, function (err, raw) {
 				if (err) return cb(err)
 					cb(null, raw.toString('hex') + path.extname(file.originalname))
 			})
 		}
 	});

 	var upload = multer({ storage: storage});

 	app.get    ('/quiz',autenticar.loginSistema, quiz.listar);
	app.post   ('/upload-img',autenticar.loginSistema, upload.single('foto'), function(req, res){
		if(req.file){
			res.json(req.file.filename);
		}
	});
	app.post   ('/quiz/busca',autenticar.loginSistema, quiz.listarRandom)
	app.post   ('/quiz',autenticar.loginSistema, quiz.cadastrar);
	app.put    ('/quiz',autenticar.loginSistema, quiz.editar);
	app.delete ('/quiz/:id',autenticar.loginSistema, quiz.deletar);
	
	app.get    ('/quiz/edit/:id',autenticar.loginSistema, quiz.editById);
	app.get    ('/responder/quiz',autenticar.loginSistema, quiz.quiz);
	app.get    ('/responder',autenticar.loginSistema, quiz.quiz);
 	app.get    ('/painel',autenticar.loginSistema, quiz.painel);

 	app.get    ('/', quiz.login)

	app.get    ('/login/facebook/', passport.authenticate('facebook', { failureRedirect: '/erro' }), quiz.facebook);
	
	app.get    ('/erro', (req,res)=>{
		res.send('erro');
	});
}