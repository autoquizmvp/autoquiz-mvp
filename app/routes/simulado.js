module.exports = function(app) {

	var simulado = app.funcao.simuladoCRUD;
	var autenticar = app.middleware.autenticador;

	app.get    ('/simulado/turma/:id',autenticar.loginSistema, simulado.listarByTurma);
	app.get    ('/simulado/professor/:id',autenticar.loginSistema, simulado.listarByProf);
	app.get    ('/simulado/autoEscola/:id',autenticar.loginSistema, simulado.listarByAutoEscola);
	app.get    ('/simulado/:id',autenticar.loginSistema, simulado.listarById);
	app.get    ('/simulado',autenticar.loginSistema, simulado.listar);
	app.put    ('/simulado',autenticar.loginSistema, simulado.editar);
	app.post   ('/simulado',autenticar.loginSistema, simulado.cadastrar);
	app.delete ('/simulado/:id',autenticar.loginSistema, simulado.deletar);

}