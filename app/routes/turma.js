module.exports = function (app){
	var turma  = app.funcao.turmaCRUD;
	var autenticar = app.middleware.autenticador;

	app.get    ('/turma',autenticar.loginSistema, turma.listar);//ok
	app.get    ('/turma/professor/:id',autenticar.loginSistema, turma.listarByProf);
	app.get    ('/turma/autoEscola/:id',autenticar.loginSistema, turma.listarByAutoEscola);
	app.get    ('/turma/:id',autenticar.loginSistema, turma.listarById);
	app.post   ('/turma',autenticar.loginSistema, turma.cadastrar);//ok
	app.put    ('/turma',autenticar.loginSistema, turma.editar);
	app.delete ('/turma/:id',autenticar.loginSistema, turma.deletar);

}