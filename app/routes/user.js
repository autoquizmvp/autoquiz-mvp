module.exports = function(app){
	var user = app.funcao.userCRUD;
	var autenticar = app.middleware.autenticador;

	app.get    ('/useListar',autenticar.loginSistema, user.listar);
	app.get    ('/user/:id',autenticar.loginSistema, user.listarById);
	app.post   ('/user',autenticar.loginSistema, user.cadastrar);
	app.put    ('/user',autenticar.loginSistema, user.editar);
	app.get    ('/user/delete/:id',autenticar.loginSistema, user.deletar);
	app.get    ('/logout',autenticar.loginSistema, function(req, res){
		req.logout();
		res.redirect('/');
	});

}