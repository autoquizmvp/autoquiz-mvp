app.controller('alunoCtrl',function($scope,$http){
	$scope.idUser = '';
	$scope.alunos = {};
	$scope.opcao = 'Salvar';
	$scope.user = {};
	$scope.aluno = {};
	$scope.aluno.autoEscola = '';
	$scope.listar = '/aluno/'
	
	$scope.verificarUser = function(_id) {
		console.log('verificarUser -> '+_id)
		$http.get('/user/'+_id)
		.then(function(result){
			if(result.data.tipo == 0) {
				$scope.show = true;
				$scope.listarAlunos($scope.listar);
			} else if(result.data.tipo == 1) {
				console.log('Sou um aluno');
				$http.get('/autoEscola/user/'+_id)
				.then(function(result){
					console.log('busquei a autoEscola')
					console.log(result.data);
					$scope.aluno.autoEscola = result.data._id;
					$scope.listar = '/aluno/autoEscola/'+result.data._id;
					$scope.listarAlunos($scope.listar);
				})
			}
		})
	}
	$scope.salvar = function(user, aluno) {
		if($scope.opcao == 'Salvar'){
			user = angular.merge(user, {tipo : 3});
			$http.post('/user', user)
			.then(function(result){
				$scope.idUser = result.data._id;
				$scope.cadastrarAluno($scope.idUser, aluno);
			})
		} else if($scope.opcao == 'Editar') {
			$http.put('/user', user)
			.then(function(result){
				$http.put('/aluno', aluno)
				.then(function(result){
					$scope.listarAlunos($scope.listar);
					$scope.aluno = {};
					$scope.user = {};
					$scope.opcao = 'Salvar';
				})
			})
		}
	}
	$scope.listarAlunos = function(url){
		$http.get(url)
		.then(function(result){
			$scope.alunos = result.data;
		})
	}
	$scope.cadastrarAluno = function(user_id, aluno){
		if(typeof aluno.autoEscola == "undefined"){
			aluno = angular.merge(aluno, {autoEscola : $scope.aluno.autoEscola});
		}
		aluno = angular.merge(aluno, {user : user_id});
		$http.post('/aluno/', aluno)
		.then(function(result){
			$scope.aluno = {};
			$scope.user = {};
			$scope.listarAlunos($scope.listar);
		})
	}
	$scope.delete = function(user, id){
		$http.delete('/aluno/'+id)
		.then(function(result){
			$http.delete('/user/'+id)
			.then(function(result){
				$scope.listarAlunos($scope.listar);
			})
		})
	}
	$scope.edit = function(id){
		$http.get('/aluno/edit/'+id)
		.then(function(result){
			$scope.opcao = 'Editar';
			$scope.listarAlunos($scope.listar);
			$scope.aluno = result.data;
			$scope.user = result.data.user;
		})
	}
	$scope.cancel = function(){
		$scope.listarAlunos($scope.listar);
		$scope.aluno = {};
		$scope.user = {};
		$scope.opcao = 'Salvar';
	}
});