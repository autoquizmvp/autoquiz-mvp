app.controller('autoCtrl',function($scope,$http){
	$scope.autoEscolas = {};
	$scope.opcao = 'Salvar';
	$scope.autoEscola = {};
	$scope.user = {};
	$scope.idUser = '';

	$scope.salvar = function(user, autoEscola) {
		if($scope.opcao == 'Salvar'){

			user = angular.merge(user, {tipo : 1});
			$http.post('/user', user)
			.then(function(result){
				$scope.idUser = result.data._id;
				$scope.cadastrarAutoEscola($scope.idUser, autoEscola);
				$scope.autoEscola = {};
				$scope.user = {};
			})

		} else if ($scope.opcao == 'Editar') {

			$http.put('/user', user)
			.then(function(result){
				$http.put('/autoEscola', autoEscola)
				.then(function(result){
					$scope.listarAutoEscolas();
					$scope.autoEscola = {};
					$scope.user = {};
					$scope.opcao = 'Salvar';
				})
			})

		}
	}
	$scope.cadastrarAutoEscola = function(id, autoEscola){
		autoEscola = angular.merge(autoEscola, {user : id});
		$http.post('/autoEscola', autoEscola)
		.then(function(result){
			$scope.listarAutoEscolas();
		})
	}
	$scope.listarAutoEscolas = function(){
		$http.get('/autoEscola')
		.then(function(result){
			$scope.autoEscolas = result.data;
		})
	}
	$scope.delete = function(id){
		$http.delete('/autoEscola/'+id)
		.then(function(result){
			$scope.listarAutoEscolas();
		})
	}
	$scope.edit = function(id){
		$http.get('/autoEscola/edit/'+id)
		.then(function(result){
			$scope.opcao = 'Editar';
			$scope.listarAutoEscolas();
			$scope.autoEscola = result.data;
			$scope.user = result.data.user;
		})
	}
	$scope.cancel = function(){
		$scope.listarAutoEscolas();
		$scope.autoEscola = {};
		$scope.user = {};
		$scope.opcao = 'Salvar';
	}
});