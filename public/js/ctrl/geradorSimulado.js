app.controller('geradorCtrl', function($scope, $http){

	$scope.categorias = [
	"Legislação do trânsito",
	"Sinalização",
	"Direção defensiva",
	"Primeiros socorros",
	"Meio ambiente e cidadania",
	"Noções de mecânica básica"
	]

	$scope.escolha = [false,false,false,false,false,false];
	$scope.gerar = true;
	$scope.lista = false;
	$scope.opcao = "Gerar simulado!";
	$scope.perguntas = {};
	$scope.simulado = {};
	$scope.simulado.autoEscola = '';
	$scope.listar = '/simulado/';
	$scope.selProf = true;

	$scope.verificarUser = function(_id) {
		console.log('verificarUser -> '+_id)
		$http.get('/user/'+_id)
		.then(function(result){
			if(result.data.tipo == 0) {
				$scope.listarSimulados($scope.listar);
				$scope.listarProfessores('/professor/');
			} else if(result.data.tipo == 1) {
				console.log('Sou uma autoEscola');
				$http.get('/autoEscola/user/'+_id)
				.then(function(result){
					console.log('busquei a autoEscola')
					console.log(result.data);
					$scope.simulado.autoEscola = result.data._id;
					console.log('Esse é o id de autoEscola -> '+$scope.simulado.autoEscola);
					$scope.listar = '/simulado/autoEscola/'+result.data._id;
					$scope.listarSimulados($scope.listar);
					$scope.listarProfessores('/professor/autoEscola/'+result.data._id);
				})
			} else if(result.data.tipo == 2){
				$http.get('/professor/user/'+_id)
				.then(function(result){
					$scope.selProf = false;
					$scope.simulado.professor = result.data._id;
					$scope.simulado.autoEscola = result.data.autoEscola._id;
					$scope.listar = '/simulado/professor/'+result.data._id;
					$scope.listarSimulados($scope.listar);
				})
			}
		})
}
$scope.delete = function(id){
	$http.delete('/simulado/'+id)
	.then(function(result){
		$scope.listarSimulados($scope.listar);
	})
}
$scope.escolher = function(){
	var count = 0;
	var length = $scope.categorias.length;
	for(i=0; i < length ; i++){
		if($scope.escolha[i] == false) {
			$scope.categorias.splice(i-count, 1);
			count++;
		}
	}

	var data = {
		categorias : $scope.categorias,
	}

	$scope.buscar(data);

	$scope.categorias = [
	"Legislação do trânsito",
	"Sinalização",
	"Direção defensiva",
	"Primeiros socorros",
	"Meio ambiente e cidadania",
	"Noções de mecânica básica"
	]
	$scope.opcao = "Gerar novamente!";

}
$scope.buscar = function(categorias){
	$http.post('/quiz/busca', categorias)
	.then(function(result){
		$scope.perguntas = result.data;
		$scope.gerar = false;
		$scope.lista = true;
	}, function(err){
		console.log(err);
	});
}
$scope.salvar = function(simulado){
	$scope.lista = false;
	$scope.opcao = "Gerar simulado!";
	if (typeof simulado.autoEscola == "undefined"){
		simulado = angular.merge(simulado, {autoEscola : $scope.simulado.autoEscola})
	}
	if (typeof simulado.professor == "undefined"){
		simulado = angular.merge(simulado, {professor : $scope.simulado.professor})
	}
	simulado = angular.merge(simulado, {questoes : $scope.perguntas})
	$http.post('/simulado', simulado)
	.then(function(result){
		$scope.simulado = {};
		$scope.listarSimulados($scope.listar);
		$scope.categorias = [
		"Legislação do trânsito",
		"Sinalização",
		"Direção defensiva",
		"Primeiros socorros",
		"Meio ambiente e cidadania",
		"Noções de mecânica básica"
		]
		$scope.escolha = [false,false,false,false,false,false];
	}, function(err){
		console.log(err);
	})
}
$scope.listarSimulados = function(url){
	$http.get(url)
	.then(function(result){
		$scope.simulados = result.data;
	})
}
$scope.listarProfessores = function(url){
	$http.get(url)
	.then(function(result){
		$scope.professores = result.data;
	})
}

});