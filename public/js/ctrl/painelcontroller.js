app.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);

app.controller('painelCtrl',function($scope, $http) {
	$scope.show = true;
	$scope.display = "none";
	$scope.questoes = {};
	$scope.questao = {};
	$scope.opcao = "Salvar";
	$scope.categoria = [
	"Legislação do trânsito",
	"Sinalização",
	"Direção defensiva",
	"Primeiros socorros",
	"Meio ambiente e cidadania",
	"Noções de mecânica básica"
	]

	$scope.upload = function(questao) {
		
		var fd = new FormData();

		if(typeof questao.foto == "undefined") {

			$scope.salvarQuestao(questao);

		} else {

			console.log('defined');
			foto = questao.foto;

			fd.append('foto', foto);

			$http.post('/upload-img', fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined},
				enctype: 'multipart/form-data'
			})
			.then(function(result){

				questao.foto = result.data;

				$scope.salvarQuestao(questao);

			})

		}

	}
	$scope.salvarQuestao = function(questao){
		if($scope.opcao=="Salvar"){

			$http.post('/quiz', questao)
			.then(function(result){
				$scope.questao = {};
				$scope.listarQuestoes();
				$scope.show = false;
			})

		} else if($scope.opcao=="Editar"){

			$http.put('/quiz',questao)
			.then(function(result){
				$scope.questao = {};
				$scope.display = "none";
				$scope.listarQuestoes();
			})
		}
	}
	$scope.listarQuestoes = function(){
		$http.get('/quiz')
		.then(function(result){
			$scope.questoes = result.data;
		})
	}
	$scope.delete = function(id){
		$http.delete('quiz/'+id)
		.then(function(result){
			$scope.listarQuestoes();
		}, function(err){
			console.log(err);
		})
	}
	$scope.edit = function(id){
		$http.get('quiz/edit/'+id)
		.then(function(result){
			$scope.listarQuestoes();
			$scope.opcao = "Editar";
			$scope.display = "";
			$scope.questao = result.data;
		}, function(err){
			console.log(err)
		})
	}
	$scope.cancel = function(){
		$scope.display = "none";
		$scope.questao = {};
		$scope.listarQuestoes();
		$scope.opcao = "Salvar";
	}
})