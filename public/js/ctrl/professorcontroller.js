app.controller('professorCtrl',function($scope,$http){
	$scope.idUser = "";
	$scope.professores = {};
	$scope.opcao = 'Salvar';
	$scope.user = {};
	$scope.professor = {};
	$scope.listar = '/professor';
	$scope.selAuto = false;
	$scope.auto_id = '';

	$scope.verificarUser = function(_id) {
		console.log('verificarUser -> '+_id)
		$http.get('/user/'+_id)
		.then(function(result){
			if(result.data.tipo == 0) {
				$scope.selAuto = true;
				$scope.listarProfessores($scope.listar);
			} else if(result.data.tipo == 1) {
				console.log('Sou uma autoEscola');
				$http.get('/autoEscola/user/'+_id)
				.then(function(result){
					console.log('busquei a autoEscola')
					console.log(result.data);
					$scope.auto_id = result.data._id;
					$scope.listar = '/professor/autoEscola/'+result.data._id;
					$scope.listarProfessores($scope.listar);
				})
			}
		})
	}
	$scope.salvar = function(user, professor) {
		console.log($scope.listar);
		if($scope.opcao == 'Salvar'){
			user = angular.merge(user, {tipo : 2});
			$http.post('/user', user)
			.then(function(result){
				$scope.idUser = result.data._id;
				console.log("A id do user é -> "+$scope.idUser);
				$scope.cadastrarProfessor($scope.idUser, professor);
				$scope.professor = {};
				$scope.user = {};
			})

		} else if ($scope.opcao == 'Editar') {
			$http.put('/user', user)
			.then(function(result){
				$http.put('/professor', professor)
				.then(function(result){
					$scope.listarProfessores($scope.listar);
					$scope.professor = {};
					$scope.user = {};
					$scope.opcao = 'Salvar';
				})
			})
		}
	}
	$scope.cadastrarProfessor = function(user_id, professor){
		if (typeof professor.autoEscola == "undefined") {
			professor = angular.merge(professor,{autoEscola : $scope.auto_id})
		}
		professor = angular.merge(professor, {user : user_id});
		$http.post('/professor/', professor)
		.then(function(result){
			$scope.listarProfessores($scope.listar);
		})
	}
	$scope.listarProfessores = function(url){
		$http.get(url)
		.then(function(result){
			$scope.professores = result.data;
		})
	}
	$scope.delete = function(id){
		$http.delete('/professor/'+id)
		.then(function(result){
			$scope.listarProfessores($scope.listar);
		})
	}
	$scope.edit = function(id){
		$http.get('/professor/edit/'+id)
		.then(function(result){
			$scope.opcao = 'Editar';
			$scope.listarProfessores($scope.listar);
			$scope.professor = result.data;
			$scope.user = result.data.user;
		})
	}
	$scope.cancel = function(){
		$scope.listarProfessores($scope.listar);
		$scope.professor = {};
		$scope.user = {};
		$scope.opcao = 'Salvar';
	}
});