app.controller('quizCtrl',function($scope,$http){
	$scope.numPergunta = 0;
	$scope.pontuacao = 0;
	$scope.perguntas = {};
	$scope.perguntaAtual = {};
	$scope.certo = 0;
	$scope.erros = 0;
	$scope.nulo = 0;
	$scope.msg = "";
	$scope.hideQuiz = true;
	$scope.opcao = "Próxima";
	$scope.resultados = {};
	$scope.showResultado = false;
	$scope.aluno = {};
	$scope.showSimulados = false;
	$scope.idSimulado = '';

	$scope.iniciarSimulado = function(id){
		$scope.idSimulado = id;
		$scope.numPergunta = 0;
		$scope.pontuacao = 0;
		$scope.certo = 0;
		$scope.erros = 0;
		$scope.nulo = 0;
		console.log("Entrei no iniciarSimulado")
		$http.get('/simulado/'+id)
		.then(function(result){
			$scope.showSimulados = true;
			$scope.perguntas = result.data.questoes;
			$scope.listarQuiz(0);
			$scope.hideQuiz = false;
		}, function(err){
			console.log(err);
		})
	}
	$scope.listarQuiz = function(num){
		$scope.numPergunta += num;
		if($scope.numPergunta <= 39) {
			if($scope.numPergunta < 0) {
				$scope.numPergunta = 0;
			}
			$scope.perguntaAtual = $scope.perguntas[$scope.numPergunta];
		}
		if($scope.numPergunta == 39) {
			$scope.opcao = "Finalizar";
		} else {
			$scope.opcao = "Próxima";
		}
		if($scope.numPergunta == 40) {
			$scope.finalizar();
		}
	}
	$scope.finalizar = function(){
		console.log("Cheguei no final!");
		var data = $scope.perguntas;
		angular.forEach(data, function(pergunta, key){
			pergunta = angular.merge(pergunta)
			if(pergunta.resposta){
				if(pergunta.resposta == pergunta.alternativaCorreta) {
					pergunta = angular.merge(pergunta, {result: 1, texto: "Você marcou a alternativa correta: "+pergunta.alternativaCorreta})
					$scope.certo+=1;
				} else {
					pergunta = angular.merge(pergunta, {result: 2, texto: "Você marcou "+pergunta.resposta+", mas a alternativa correta é: "+pergunta.alternativaCorreta})
					$scope.erros+=1;
				}
			} else {
				$scope.nulo+=1;
				pergunta = angular.merge(pergunta, {result: 3, texto: "Você não marcou nenhuma alternativa. A alternativa correta é: "+pergunta.alternativaCorreta})
			}
		})
		$scope.resultados = data;
		$scope.pontuacao = $scope.certo*0.25;
		if($scope.pontuacao < 7.00){
			$scope.msg = "Você ainda precisa melhorar!";
		} else {
			$scope.msg = "Parabéns, você teve um bom resultado!"
		}
		$scope.hideQuiz = true;
		$scope.showResultado = true;
		$scope.perguntas = {};
	}
	$scope.concluir = function(id){
		var data = {};
		console.log("Conclusão -> "+id);
		$http.get('/aluno/'+id)
		.then(function(result){
			$scope.aluno = result.data;
			data = {
				simulado : $scope.idSimulado,
				aluno : $scope.aluno._id,
				autoEscola : $scope.aluno.autoEscola,
				nota : $scope.pontuacao
			}
			console.log(data);
			$http.post('/nota', data)
			.then(function(result){
				$scope.showResultado = false;
				$scope.showSimulados = false;
			})
		})
	}
});