app.controller('templateCtrl', function($scope, $http) {
	$scope.template = {};
	$scope.template.local = '';
	$scope.autoEscolaU = {}; 
	$scope.professorU = {};
	$scope.alunoU = {};
	$scope.user_id = {};
	$scope.turmas = {};
	$scope.professores = {};
	$scope.selAuto = false;
	$scope.aluno_id = '';

	$scope.listaTurma = function(url){
		$http.get('/turma/'+url)
		.then(function(result){
			$scope.turmas = result.data;
		}, function(err){
			console.log(err);
		});
	}
	$scope.listaProfessor = function(url){
		$http.get('/professor/'+url)
		.then(function(result){
			$scope.professores = result.data;
		}, function(err) {
			console.log(err);
		})
	}
	$scope.listaSimulado = function(url){
		$http.get('/simulado/'+url)
		.then(function(result){
			$scope.simulados = result.data;
		})
	}
	$scope.listaAluno = function(url){
		$http.get('/aluno/'+url)
		.then(function(result){
			$scope.alunos = result.data;
		})
	}
	$scope.listaAutoEscola = function(url){
		$http.get('/autoEscola/'+url)
		.then(function(result){
			$scope.autoEscolas = result.data;
		})
	}
	$scope.adminU = function(id){
		console.log('Sou admin -> '+id);
		$scope.listaAluno('');
		$scope.listaTurma('');
		$scope.listaProfessor('');
		$scope.listaAutoEscola('');
		$scope.selAuto = true;
	}
	$scope.autoEscolaU = function(id){
		console.log("Sou autoEscola -> "+id);
		$http.get('/autoEscola/user/'+id)
		.then(function(result){
			var url = 'autoEscola/'+result.data._id;
			$scope.listaAluno(url);
			$scope.listaTurma(url);
			$scope.listaProfessor(url);
		}, function(err){
			console.log(err);
		});
	}
	$scope.professorU = function(id){
		console.log("Sou professor -> "+id);
		$http.get('/professor/user/'+id)
		.then(function(result){
			console.log(result.data);
			$scope.professorU = result.data;
			var url = 'professor/'+result.data._id;
			$scope.listaTurma(url);
		}, function(err){
			console.log(err);
		});
	}
	$scope.alunoU = function(id){
		console.log("Sou aluno -> "+id);
		$http.get('/aluno/user/'+id)
		.then(function(result){
			$scope.aluno_id = result.data._id;
			var url = 'turma/'+result.data.turma;
			$scope.listaSimulado(url);
		}, function(err){
			console.log(err);
		});
	}
	$scope.find = function(id) {
		$http.get('/user/'+id)
		.then(function(result){
			$scope.user_id = result.data;
			if($scope.user_id.tipo==0){
				$scope.adminU($scope.user_id._id);
			} else if ($scope.user_id.tipo==1){
				$scope.autoEscolaU($scope.user_id._id);
			} else if ($scope.user_id.tipo==2){
				$scope.professorU($scope.user_id._id);
				console.log('sou professor');
			} else if ($scope.user_id.tipo==3){
				$scope.alunoU($scope.user_id._id);
			}
			$scope._id = $scope.user_id._id;
		})
	}
})