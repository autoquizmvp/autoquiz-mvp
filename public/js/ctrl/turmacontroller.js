app.controller('turmaCtrl',function($scope,$http){
	$scope.turma = {};
	$scope.turmas = {};
	$scope.listar = '/turma/';
	$scope.turma.autoEscola = '';
	$scope.opcao = 'Salvar';

	$scope.verificarUser = function(_id) {
		console.log('verificarUser -> '+_id)
		$http.get('/user/'+_id)
		.then(function(result){
			if(result.data.tipo == 0) {
				$scope.listarTurmas($scope.listar);
				console.log('admin na turma');
			} else if(result.data.tipo == 1) {
				console.log('Sou uma autoEscola');
				$http.get('/autoEscola/user/'+_id)
				.then(function(result){
					console.log('busquei a autoEscola');
					$scope.turma.autoEscola = result.data._id;
					$scope.listar = '/turma/autoEscola/'+result.data._id;
					$scope.listarTurmas($scope.listar);
				})
			}
		})
	}
	$scope.salvar = function(turma){
		if($scope.opcao == 'Salvar'){
			if(typeof turma.autoEscola == "undefined"){
				turma = angular.merge(turma, {autoEscola : $scope.turma.autoEscola});
			}
			$http.post('/turma', turma)
			.then(function(result){
				$scope.turma = {};
				$scope.listarTurmas($scope.listar);
			})
		} else if($scope.opcao == 'Editar'){
			$http.put('/turma', turma)
			.then(function(result){
				$scope.turma = {};
				$scope.listarTurmas($scope.listar);
			})
		}
	}
	$scope.delete = function(id){
		$http.delete('/turma/'+id)
		.then(function(result){
			$scope.listarTurmas($scope.listar);
		})
	}
	$scope.listarTurmas = function(url){
		$http.get(url)
		.then(function(result){
			$scope.turmas = result.data;
		})
	}
	$scope.edit = function(id){
		$scope.opcao = 'Editar';
		$http.get('/turma/'+id)
		.then(function(result){
			$scope.turma = result.data;
			$scope.listarTurmas($scope.listar);
		})
	}
})